#include<iostream>
#include<stack>
#include<string>

using namespace std;
class mypre
{
private:
    /* data */
public:
/**
 * to check wheather it is operand
 */
bool isOperand(char val)
{
    if (val>='0' && val<='9')
    {
        /* code */
        return true;
    }
    if((val>='A' && val<='Z')|| (val>='a' && val<='z'))
    {
        return true;
    }
    return false;
    
    
}
/**
 * to check the operators in the stack
 */
bool isOperator(char val)
{
    if (val=='+'|| val=='-'||val=='*'||val=='/')
    {
        /* code */
        return true;
    }
    
}
/*
    to check the right associatve in the stack 
*/
bool isRightAssosiative(char val)
{
    if (val=='$')
    {
        /* code */
        return true;
    }
    return false;
    
}
/**
 * to check the weight of the operator to print according;y the presedence
 */
int weightChecker(char val)
{
    int operator_weight=-1;
    switch (val)
    {
        case '+':
        case '-':
        operator_weight=1;
        case '*':
        case '/':
        operator_weight=2;
        case '$':
        operator_weight=3;
    }
    return operator_weight;
}

/**
 * method to check the precendece of the inputs
 */
int precedenceChecker(char val1,char val2)
{
    int weight1=weightChecker(val1);
    int weight2=weightChecker(val2);
    if (weight1==weight2)
    {
        /* code */
        if(isRightAssosiative(val1))
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }
    return weight1 > weight2 ? true :false;
    
}

string conversionLogic(string input)
{
    stack<char> charstack;
    string res=" ";
    for (int i = 0; i < input.length(); i++)
    {
        /* code */
        if (input==" " || input == ",")
        {
            /* code */
            continue;
        }
        else if (isOperator(input[i]))
        {
            /* code */
            while (!charstack.empty()&&charstack.top()!="("&&precedenceChecker(charstack.top(),input[i]))
            {
                /* code */
                res+=charstack.top();
                charstack.pop();
            }
            charstack.push(input[i]);
        }

        else if (isOperand(input[i]))
        {
            /* code */
            res+=input[i];
        }
        else if (input[i]=='(')
        {
            /* code */
            charstack.push(input[i]);
        }
        else if (input[i]==')')
        {
            /* code */
            while (!charstack.empty() && charstack.top() != '(')
            {
                /* code */
                res=charstack.top();
                charstack.pop();
            }
            charstack.pop();
            
        }
    }
    while (!charstack.empty())
    {
        /* code */
        res+=charstack.top();
        charstack.pop();
    }

    return res;
    
    

}

};

int main()
{
    /* code */
    mypre mp;
    string input;
    cout<<"\n Enter the Expression  \n ";
    getline(cin,input);
    string output=mp.conversionLogic(input);
    cout<<"\n After postfix: "<<output<<"\n ";

    return 0;
}

