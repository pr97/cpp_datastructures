#include<iostream>
#include<time.h>
using namespace std;
class MergeSort
{
public:
    void mergehelper(int *x,int s,int e)
    {
        int mid=(s+e)/2;
        int i = s;
        int j= mid+1;
        int k= s;
        int temp[100];
        while(i<=mid && j<=e)
        {
            if(x[i]<x[j]) /** Shiting of the array from on index to another index happens here **/
            {
                temp[k++] = x[i++];
            }
            else
            {
                temp[k++] = x[j++];
            }
        }
        while (i<=mid)
        {
            /* code */
            temp[k++] = x[i++];
        }
        while (j<=e)
        {
            /* code */
            temp[k++] = x[j++];
        }
        /** the below code to used to copy the temporary elements to original array i.e, x[] this lopp is similar to for each loop in java**/
        for (int  original = s; original<=e; original++)
        {
            /* code */
            x[original] = temp[original];
        }
    }
    void mergeSortingLogic(int x[],int s,int e)
    {
        if (s>=e)
        {
            /*This code is executed if their is 1 or 0 element in the array */
            return;
        }
            int mid= (s+e)/2;
            mergeSortingLogic(x,s,mid);
            mergeSortingLogic(x,mid+1,e);
            mergehelper(x,s,e);
    }


};

int main()
{
    MergeSort ms;
    int user_choice;
    cout<<"\n Enter the array size: ";
    cin>>user_choice;
    int ele[user_choice];
    cout<<"\n Enter the array elements"<<"\n";
    for (int i = 0; i < user_choice; i++)
    {
        cin>>ele[i];
    }
    cout<<"\n Array before sorting";
    for (int i = 0; i < user_choice; i++)
    {
        cout<<"\t "<<ele[i];
    }
    ms.mergeSortingLogic(ele,0,user_choice-1);
    cout<<"\n Array after sorting";
    for (int i = 0; i < user_choice; i++)
    {
        cout<<"\t "<<ele[i];
    }

    cout<<"\n";
    cout<<"\n Eureka..............";

    return 0;
}

