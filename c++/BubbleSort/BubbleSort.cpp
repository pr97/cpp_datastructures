#include<iostream>
using namespace std;
class BubbleSort
{
private:
    /* data */
public:
    int user_choice;
    void bubbleArraySearch()
    {
        cout<<"\n Enter the size of the array";
        cin>>user_choice;
        int a[user_choice];
        cout<<"\n Enter the array element";
        for(int i=0;i<user_choice;i++)
        {
            cin>>a[i];
        }
        cout<<"\n Array Before Sorting";
        for (int i = 0; i < user_choice; i++)
        {
            /* code */
            cout<<"\n "<<a[i]<<"\n";
        }
        for (int i = 0; i < user_choice; i++)
        {
            /* code */
            for(int j=0;j<user_choice;j++)
            {
                if(a[i]<a[j])
                {
                    int temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        cout<<"\n Array After sorting:";
        for (int i = 0; i < user_choice; i++)
        {
            /* code */
            cout<<"\n "<<a[i]<<"\n";
        }
    }
        
        
};

int main(int argc, char const *argv[])
{
    /* code */
    BubbleSort Bs;
    Bs.bubbleArraySearch();
    return 0;
}

