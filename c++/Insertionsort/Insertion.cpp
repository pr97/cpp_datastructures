#include<iostream>
using namespace std;
int main()
{
    /* code */
    int user_choice;
    cout<<"\n Enter the size of the array";
        cin>>user_choice;
        int a[user_choice];
        cout<<"\n Enter the array element";
        for(int i=0;i<user_choice;i++)
        {
            cin>>a[i];
        }
        /**
         * Logic starts here
         */
        for (int i = 1; i < user_choice; i++) //   @ rahul : why i=1? because in insertion sort we compare 1st index element with previous one
        {
            /* code */
            int temp=a[i]; //@ rahul: i=1 so temp stores the element of index num 1 of the array
            int j=i-1; // @ rahul: this line is for companing with the previous element now the values of j=0 
            while (j>=0 && a[j]>temp)  //@ rahul:  j>=0 is using to check till the element index reaches -1
            {
                /* code */
                a[j+1]=a[j];
                j=j-1; // @ rahul: now the j value be j=-1 for the next loop 
            }
            a[j+1]=temp; //@ rahul:  as j value is -1 currently -1+1= 0
            

        }

        for(int i=0;i<user_choice;i++)
        {
            cout<<"\n"<<a[i]<<"\n";
        }
        
    return 0;
}
