#include<iostream>
using namespace std;
class Qs
{
private:
    /* data */
public:
    int partition(int *a,int s, int e)
    {
        int i=s-1;
        int j=s;
        int pivot /**aka mid**/ =a[e];
        for (; j <e; j++)
        {
            /* code */
            if(a[j]>=pivot)
            {
                i++;
                swap(a[i],a[j]);
            }
        }
        swap(a[i+1],a[e]);
        return i+1;
        
    }
    void qucickSortLogic(int *a,int s,int e)
    {
        if (s>=e)
        {
            /* code */
            return;
        }
        int pivot=partition(a,s,e);
        qucickSortLogic(a,s,pivot-1);
        qucickSortLogic(a,pivot+1,e);
        
    }

};

int main()
{
    /* code */
    Qs quick;
    int user_choice;
    cout<<"\n Enter the size of the Array: ";
    cin>>user_choice;
    int a[user_choice];
    for (int i = 0; i < user_choice; i++)
    {
        /* code */
        cin>>a[i];
    }
    cout<<"\n Array before sorting";
    for (int i = 0; i < user_choice; i++)
    {
        /* code */
        cout<<"\t "<<a[i];
    }
    quick.qucickSortLogic(a,0,user_choice-1);
    cout<<"\n Array after Sorting";
      for (int i = 0; i < user_choice; i++)
    {
        /* code */
        cout<<"\t "<<a[i];
    }

    cout<<"\n ";

    
    return 0;
}

